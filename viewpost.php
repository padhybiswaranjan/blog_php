<?php require('includes/config.php'); 


//uses prepared statement; fetches the post whose post ID was passed from homepage as query string
$stmt = $db->prepare('SELECT postID, postTitle, postCont, postDate FROM blog_posts WHERE postID = :postID');
$stmt->execute(array(':postID' => $_GET['id'])); //fetches ID passed from homepage and assigns it to postID
$row = $stmt->fetch();

//using prepared statement is better than executing a query directly as the items are bound in the array and sent directly to server
//this does not allow tampering of database and gives better security

//if post does not exists redirect user to homepage
if($row['postID'] == ''){
	header('Location: ./');
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blog - <?php echo $row['postTitle'];?></title> <!-- display title of the post -->
    <link rel="stylesheet" href="style/normalize.css">
	<link rel="stylesheet" href="style/main.css">
	<link rel="stylesheet" href="style/bootstrap.css">
</head>
<body>

<!-- header -->
<div class="banner">
<div class="container">
	<div class="header">
		<div class="logo">
			<a href="index.html"><img src="images/logo.png" class="img-responsive" alt="" /></a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="head-nav">
		<span class="menu"></span>
		<ul class="cl-effect-15">
			<li class="active"><a href="./">HOME</a></li>
			<li><a href="./admin/index.php" data-hover="ADMIN">ADMIN</a></li>
			<div class="clearfix"> </div>
		</ul>
	</div>
	<!-- script-for-nav -->
	<script>
		$( "span.menu" ).click(function() {
					  $( ".head-nav ul" ).slideToggle(300, function() {
						// Animation complete.
					  });
			});
	</script>		 
</div>
</div> <!-- ./ header -->

<!--content-->
<div class="content">
	<div class="container">
		<?php	//display title, content along with date-time
			echo '<div>';
				echo '<h1>'.$row['postTitle'].'</h1>';
				echo '<p>Posted on '.date('jS M Y', strtotime($row['postDate'])).'</p>';
				echo '<p>'.$row['postCont'].'</p>';				
			echo '</div>';
		?>
	</div>
</div> <!-- ./ content-->

</body>
</html>
