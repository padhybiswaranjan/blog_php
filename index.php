<?php require('includes/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <link rel="stylesheet" href="style/normalize.css">
	<link rel="stylesheet" href="style/main.css">
	<link rel="stylesheet" href="style/bootstrap.css">
</head>
<body>

<!-- header -->
<div class="banner">
	<div class="container">
		<div class="header">
			<div class="logo">
				<a href="index.html"><img src="images/logo.png" class="img-responsive" alt="" /></a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="head-nav">
			<span class="menu"></span>
			<ul class="cl-effect-15">
				<li class="active"><a href="./">HOME</a></li>
				<li><a href="./admin/index.php" data-hover="ADMIN">ADMIN</a></li>
				<div class="clearfix"> </div>
			</ul>
		</div>
		<!-- script-for-nav -->
		<script>
			$( "span.menu" ).click(function() {
						  $( ".head-nav ul" ).slideToggle(300, function() {
							// Animation complete.
						  });
				});
		</script>		 
	</div>
</div> <!-- ./ header -->

<!--content-->
<div class="content">
	<div class="container">	
		<?php
			try {
					$stmt = $db->query('SELECT postID, postTitle, postDesc, postDate FROM blog_posts ORDER BY postID DESC'); //fetch data from blog_post in descending orded of ID i.e from latest to first
					while($row = $stmt->fetch()){ 
						echo '<div>';
						echo '<h3><a href="viewpost.php?id='.$row['postID'].'">'.$row['postTitle'].'</a></h3>'; //ID is passed as query string to open detail post page[viewpost.php] when read more is clicked.
						echo '<p>Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).'</p>'; //fetch date and time is HH:MM:SS
						echo '<p>'.$row['postDesc'].'</p>';				//fetch description
						echo '<p><a href="viewpost.php?id='.$row['postID'].'">Read More</a></p>';	
						echo '<hr style=" display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0; padding: 0;">';			
						echo '</div>';
					}//loop to display title, date, descriptn for each of the fetched post
				} catch(PDOException $e) {
			   		echo $e->getMessage();
					}
		?>
	</div> <!-- ./ container-->
</div> <!-- ./ content-->

</body>
</html>