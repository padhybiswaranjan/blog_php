<?php
ob_start();
session_start();

//change database credentials here as required
define('DBHOST','localhost');
define('DBUSER','root');
define('DBPASS','root');
define('DBNAME','blog');

$db = new PDO("mysql:host=".DBHOST.";port=8889;dbname=".DBNAME, DBUSER, DBPASS); //change port number if required; currently set to default port of mamp
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//set timezone
date_default_timezone_set('IST');

//load classes as needed
function __autoload($class) {
   
   $class = strtolower($class); //converts class names to lower case

	//if call from root
   $classpath = 'classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath; //checks if files exists; if yes the include the file
	} 	
	
	//if call from within directory
   $classpath = '../classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath;
	}
	
	//if call from directory within directory 
   $classpath = '../../classes/class.'.$class . '.php';
   if ( file_exists($classpath)) {
      require_once $classpath;
	} 		
	 
}

$user = new User($db); //providing access to database
?>