# General Overview #

## Read comments in code for detailed explanation of each step ##

There are two tables in the database - blog\_post and blog\_members.

blog\_post has post ID(PK), title, description, content and date. ID is set to auto increment with each post and date is appended using the timezone set in config.php.  Title, description and content are fetched from form.
 
blog\_members has member ID(PK) , username, pass and email. ID is auto increment with each new registered user.

db.sql is the database dump. It might still have some data that I added to test. Admin id pass is ‘admin’, ‘admin’

config.php initializes database connection, loads all the classes using autoload() and sets timezone.

index.php is the homepage. Lists all existing post’s title, date-time, description and link for admin login. Post description is visible only here and not on detailed page.

viewpost.php is the detailed page that displays a post that is clicked from home page.

All the admin scripts are under /admin. All the php scripts first load config.php to check if the admin($user) is logged in. If admin is logged in then index.php[/admin] is loaded, if not then it will redirect to login page.

login.php[/admin] displays a form containing username and password field for admin login.

index.php[/admin] lists all posts in a table with option to edit/delete/create a post.

menu.php[/admin] is a UL of links. Blog open index.php[/admin], Users opens user.php that displays list of users, View website opens index.php[root] and Logout runs the logout() from logout.php

add-post.php[/admin] TinyMCE editor is used for the form of post creation.

edit-post.php[/admin]  Same as add-post but first s script is run to check which postID is to be edited.

users.php[/admin] Displays list of users in table format.

add-user.php[/admin] same as add-post but TinyMCE editor is not used.

edit-user.php[/admin] same as edit-post except for a few changes for security(refer comments in code).


### Follow this sequence to read comments in the code for better understanding: ###
> 1. includes/config.php
> 2. /index.php
> 3. /viewpost.php
> 4. admin/login.php
> 5. classes/class.user.php
> 6. admin/index.php
> 7. admin/menu.php
> 8. admin/add-post.php
> 9. admin/edit-post.php
> 10. admin/users.php
> 11. admin/add-user.php
> 12. admin/edit-user.php