<!--used for login&logout and create hash for passwords-->
<?php

include('class.password.php');

class User extends Password{

    private $db;
	//constructor creates a database connection and assigns it to a variable db for access
	function __construct($db){
		parent::__construct();

		$this->_db = $db;
	}

	//checks if user is logged in. If the loggedin session is SET, then it'll return true else nothing is returned.
	public function is_logged_in(){
		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
			return true;
		}
	}

	//fetches blog_members data from database
	private function get_user_hash($username){

		try {

			$stmt = $this->_db->prepare('SELECT MemberID, username, password FROM blog_members WHERE username = :username');
			$stmt->execute(array('username' => $username));

			return $stmt->fetch();

		} catch(PDOException $e) {
		    echo '<p class="error">'.$e->getMessage().'</p>';
		}
	}


	public function login($username,$password){
		$user = $this->get_user_hash($username); //binds data received by get_user_hash
		//matches the $password from the login form and the hash from password field of database($user['password']). If it matches; returns 1
		if($this->password_verify($password,$user['password']) == 1){ //if 1 is returned loggedin session is SET

		    $_SESSION['loggedin'] = true;
		    $_SESSION['memberID'] = $user['memberID'];
		    $_SESSION['username'] = $user['username'];
		    return true;
		}
	}


	public function logout(){
		session_destroy();
	}

}


?>
